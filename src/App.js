import React, { Component } from "react";
import { connect } from "react-redux";

import MainComponent from "./components/MainComponent";
import LoginPage from "./components/forms/LoginPage";


const mapStateToProps = (state) => {
	return {
		loggedIn: state.loggedIn
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
	};
};

class App extends Component {
	// constructor(props) {
	// 	super(props);
	// }

	render() {
		let component = undefined;

		if (!this.props.loggedIn) {
			component = <LoginPage />
		} else {
			// component = <h1>Logged in main thing</h1>
			component = <MainComponent />
		}

		return (
			<div id="wrapper">
				{component}
			</div>
		)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
