import React, { Component } from "react";
import { connect } from "react-redux";

import "./styles/MainComponent.css";
import Navbar from "./navigation/Sidebar";
import DictionaryTable from "./forms/DictionaryTable";
import AccountStatement from "./pages/AccountStatement";
import Dashboard from "./pages/Dashboard";
import { Container } from "react-bootstrap";
import { pages } from "../store/misc/global";

const mapStateToProps = (state) => {
	return {
		activePage: state.activePage,
	};
};

const mapDispatchToProps = (dispatch) => {
	return {};
};

/*
	Overall application has following functionalities
	
	- patients
	-+ register new patient and create basic profile
	-+ update profile on different levels

	- users
	-+ login, register, profile

*/

class MainComponent extends Component {
	// constructor(props) {
	// 	super(props);

	// }


	render() {
		// let activePage = <DictionaryTable/>;
		let activePage;
		if(this.props.activePage === pages.dashboard){
			activePage = <Dashboard/>;
		}
		if(this.props.activePage === pages.dictionary){
			activePage = <DictionaryTable/>;
		}
		if(this.props.activePage === pages.accountStatement){
			activePage = <AccountStatement />;
		}

		return <>
			<Navbar />
			<div id="content-wrapper" className="d-flex flex-column pt-3">
				<div id="content">
					<Container fluid>
						{activePage}
					</Container>
				</div>
			</div>
		</>;
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(MainComponent);
