import React, { Component } from "react";
import { connect } from "react-redux";
import { getDictionaryAction } from "../../store/actions/Actions";

const mapStateToProps = (state) => {
    return {
        dictionaryValues: state.dictionary.values,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getDictionary: () => dispatch(getDictionaryAction())
    };
};

class DictionaryTable extends Component {
    constructor(props) {
        super(props);
        console.log("hangon")

        this.parseDictionaryJsx = this.parseDictionaryJsx.bind(this);

        let dictionaryRows = [];
        if (this.props.dictionaryValues === undefined) {
            this.props.getDictionary();
        } else {
            dictionaryRows = this.parseDictionaryJsx();
        }

        this.state = {
            dictionaryRows,
        }
    }

    componentDidUpdate(prevPropes, prevState) {
        if (JSON.stringify(prevPropes.dictionaryValues) === JSON.stringify(this.props.dictionaryValues))
            return;
        this.parseDictionaryJsx();
    }

    parseDictionaryJsx() {
        const dictionaryRows = !this.props.dictionaryValues ? undefined : this.props.dictionaryValues.map(
            (row) => {
                return <tr>
                    <td width="30%">
                        <span>{row.english}</span>
                    </td>
                    <td width="30%">
                        <span>{row.arabic}</span>
                    </td>
                </tr>;
            }
        );

        this.setState({
            dictionaryRows
        });
    }

    render() {
        let component =
            <>
                <div className="card mb-12">
                    <div className="card-header">Dictionary</div>
                    <div className="card-body">
                        <div className="table-responsive">
                            <table className="table table-bordered table-striped table-sm" id="dataTable" width="100%"
                                cellSpacing="0">
                                <thead className="thead-dark">
                                    <tr>
                                        <th>English</th>
                                        <th>Arabic</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    {this.state.dictionaryRows}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div >
            </>;
        return component;
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DictionaryTable);