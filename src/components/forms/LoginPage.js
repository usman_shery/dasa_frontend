import React, { Component } from "react";

import { connect } from "react-redux";
import { loginUserAction } from "../../store/actions/Actions";
import { validation } from "../../store/misc/global";

const mapStateToProps = (state) => {
	return {};
};

const mapDispatchToProps = (dispatch) => {
	return {
		login: (username, password) => dispatch(loginUserAction(username, password))
	};
};

class LoginPage extends Component {
	constructor(props) {
		super(props);

		this.state = {
			form: {
				username: "",
				password: "",
			},
			error: {
				username: false,
				password: false,
			},
			labels: {
				username: "User Name",
				password: "Password",
			}
		}

		this.setFormValue = this.setFormValue.bind(this);
		this.triggerAction = this.triggerAction.bind(this);
	}

	setFormValue(id, value) {
		if (id === "username" && validation.hasSpaces(value)) {
			return;
		}

		this.setState({
			...this.state,
			form: {
				...this.state.form,
				[id]: value
			}
		})
	}

	triggerAction() {
		// validate: user has entered the values or not?
		if (validation.isNull(this.state.form.username) || validation.isNull(this.state.form.password)) {
			alert("Complete your form before submission");
			console.log(this.state.form);
			return;
		}
		let formVales = this.state.form;
		this.props.login(formVales.username, formVales.password);
	}

	render() {
		let component =
			<>
				<div className="container">
					<div className="row justify-content-center">
						<div className="col-xl-4 col-lg-12 col-md-6">
							<div className="text-center pt-5">
							</div>
							<div className="card o-hidden border-0 shadow-lg my-5">
								<div className="card-body p-0">
									<div className="row">
										<div className="col-lg-12">
											<div className="p-5">
												<div className="text-center">
													<h1 className="h6 text-gray-900 font-weight-bold mb-4">Log in to your account</h1>
												</div>
												<div className="form-group">
													<input type="text" className="form-control form-control-user" aria-describedby="emailHelp" placeholder="Username..." id="username" name="username" autoFocus=""
													value={this.state.form.username}
													onChange={(event) => { this.setFormValue("username", event.target.value); }}
													/>
												</div>
												<div className="form-group">
													<input type="password" className="form-control form-control-user" placeholder="Password" id="password" name="password"
														value={this.state.form.password}
														onChange={(event) => { this.setFormValue("password", event.target.value); }}
													/>
												</div>
												<div className="form-group">
													<div className="custom-checkbox small">
														<input type="submit" className="btn btn-primary btn-user btn-block" value="Login" id="customCheck"
														onClick={() => {this.triggerAction();}}
														/>
													</div>
													<hr />
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<script src="/vendor/jquery/jquery.min.js"></script>
					<script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
					<script src="/vendor/jquery-easing/jquery.easing.min.js"></script>
					<script src="/js/sb-admin-2.min.js"></script>
				</div>
			</>;
		return component;
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);