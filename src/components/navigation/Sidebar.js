import React, { Component } from "react";
import { connect } from "react-redux";

import { changePageAction } from "../../store/actions/Actions";
import { pages } from "../../store/misc/global";

const mapStateToProps = (state) => {
	return {
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		changePage: (page) => dispatch(changePageAction(page))
	};
};

class Navbar extends Component {
	// constructor(props) {
	// 	super(props);
	// }

	render() {
		let component =
			<ul className="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

				<div className="sidebar-brand d-flex align-items-center justify-content-center" onClick={() => this.props.changePage(pages.dashboard)}>
					<div className="sidebar-brand-icon rotate-n-15">
						<i className="fab fa-accusoft"></i>
					</div>
					<div className="sidebar-brand-text mx-3">Arabic Statement</div>
				</div>

				<hr className="sidebar-divider my-0" />

				<li className="nav-item active">
					<div className="nav-link" onClick={() => this.props.changePage(pages.dashboard)}>
						<i className="fas fa-fw fa-tachometer-alt"></i>
						<span>Dashboard</span>
					</div>
				</li>

				<hr className="sidebar-divider" />

				<div className="sidebar-heading">
					Interface
				</div>

				<li className="nav-item">
					<div className="nav-link" onClick={() => this.props.changePage(pages.dictionary)}>
						<i className="fas fa-fw fa-chart-area"></i>
						<span>Manage Params</span>
					</div>
				</li>
				<li className="nav-item">
					<div className="nav-link" onClick={() => this.props.changePage(pages.accountStatement)}>
						<i className="fas fa-fw fa-chart-area"></i>
						<span>Account Statement</span>
					</div>
				</li>
				<li className="nav-item">
					<div className="nav-link" href="logout">
						<i className="fas fa-fw fa-chart-area"></i>
						<span>Logout</span>
					</div>
				</li>


				<hr className="sidebar-divider d-none d-md-block" />

				<div className="text-center d-none d-md-inline">
					<button className="rounded-circle border-0" id="sidebarToggle"></button>
				</div>

			</ul>;

		return component;


	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Navbar);
