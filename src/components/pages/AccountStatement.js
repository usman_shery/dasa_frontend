import React, { Component } from "react";
import { Row } from "react-bootstrap";
import { connect } from "react-redux";
import { getAccountStatementAction } from "../../store/actions/Actions";

const mapStateToProps = (state) => {
    return {
        statementDetails: state.statement.values
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getAccountStatement: (start, end, accountNumber) => dispatch(getAccountStatementAction(start, end, accountNumber))
    };
};

class AccountStatement extends Component {
    constructor(props) {
        super(props);

        this.state = {
            form: {
                startDate: "",
                endDate: "",
                accountNumber: "",
            },
            error: {
                startDate: false,
                endDate: false,
                accountNumber: false,
            },
            labels: {
                startDate: "",
                endDate: "",
                accountNumber: "",
            }
        }

    }

    componentDidUpdate(prevPropes, prevState) {
        if (JSON.stringify(prevPropes.statementDetails) === JSON.stringify(this.props.statementDetails))
            return;
        let dictionaryRows = this.loadStatemenetJsx();
        this.state = {
            dictionaryRows,
        }
    }

    loadStatemenetJsx() {
        let statementTableRows = undefined;

        if (this.props.statementDetails) {
            statementTableRows = this.props.statementDetails.map(
                (statementRow) => {
                    return <tr>
                        <td width="30%">
                            <span>{statementRow.valueDate}</span>
                        </td>
                        <td width="30%">
                            <span>{statementRow.postDate}</span>
                        </td>
                        <td width="30%">
                            <span>{statementRow.referenceNo}</span>
                        </td>
                        <td width="30%">
                            <span>{statementRow.transactionAmount}</span>
                        </td>
                        <td width="30%">
                            <span>{statementRow.transactionType}</span>
                        </td>
                        <td width="30%">
                            <span>{statementRow.creditDebit}</span>
                        </td>
                        <td width="30%">
                            <span>{statementRow.balance}</span>
                        </td>
                    </tr>;
                }
            );
        }

        return statementTableRows;

    }

    setFormValue(id, value) {
        this.setState({
            ...this.state,
            form: {
                ...this.state.form,
                [id]: value
            }
        })
    }

    triggerAction() {
        // any validation here
        let form = this.state.form;
        this.props.getAccountStatement(form.startDate, form.endDate, form.accountNumber);
    }

    render() {
        let component =
            <>
                <div className="card shadow mb-4">
                    <div className="card-header py-3">
                        <h6 className="m-0 font-weight-bold text-primary">Arabic</h6>
                    </div>
                    <div className="card-body">
                        <div className="form-row">
                            <div className="form-group col-md-6">
                                <label>Start Date (DD-MM-YYYY)</label>
                                <input type="date" className="form-control form-control-user" placeholder="Start Date" id="startDate" name="startDate"
                                    value={this.state.form.startDate}
                                    onChange={(event) => { this.setFormValue("startDate", event.target.value); }}
                                />
                            </div>
                            <div className="form-group col-md-6">
                                <label>End Date (DD/MM/YYYY)</label>
                                <input type="date" className="form-control form-control-user" placeholder="End Date" id="endDate" name="endDate"
                                    value={this.state.form.endDate}
                                    onChange={(event) => { this.setFormValue("endDate", event.target.value); }}
                                />
                            </div>
                            <div className="form-group col-md-6">
                                <label>Account Number</label>
                                <input type="text" className="form-control form-control-user" placeholder="Account Number" id="accountNumber" name="accountNumber"
                                    value={this.state.form.accountNumber}
                                    onChange={(event) => { this.setFormValue("accountNumber", event.target.value); }} F
                                />
                            </div>
                        </div>
                        <p><input type="submit" value="Search" className="btn btn-info" onClick={() => { this.triggerAction() }} /></p>
                        <Row>
                            <div className="col-12 text-right mb-2">
                                <a href="/downloadArabic" className="d-none d-sm-inline-block btn btn-sm btn-info shadow-sm"><i className="fas fa-file fa-sm text-white-50"></i> Download Arabic</a>
                            </div>

                            <div className="col-12 text-right mb-2">
                                <a href="/downloadEnglish" className="d-none d-sm-inline-block btn btn-sm btn-info shadow-sm"><i className="fas fa-file fa-sm text-white-50"></i> Download English</a>
                            </div>
                        </Row>
                        <div className="table-responsive">
                            <table className="table table-bordered table-striped table-sm" id="dataTable" width="100%" cellSpacing="0">
                                <thead className="thead-dark">
                                    <tr>
                                        <th>Value Date</th>
                                        <th>Transaction Name</th>
                                        <th>Post Date</th>
                                        <th>Debit</th>
                                        <th>Credit</th>
                                        <th>Balance</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    {this.state.statementTableRows}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </>;
        return component;
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AccountStatement);