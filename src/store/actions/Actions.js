import { onPageChange, onUserLogin, onDictionaryGet, onAccountStatementGet, onFailure } from "../session";
import { apiCall, httpHeaders, httpMethods } from "../middleware/api";

const loginUrl = "auth/login";
const dictionaryGetUrl = "dictionary";
const statementGetUrl = "statement";

export const changePageAction = (page) => onPageChange({ page });

export const loginUserAction = (username, password) =>
	// onUserLogin({username, password})
	apiCall({
		url: loginUrl,
		callParams: {
			method: httpMethods.post,
			headers: httpHeaders,
			// credentials: "include",
			body: JSON.stringify({
				username,
				password
			}),
		},
		onSuccess: onUserLogin.type,
		onFailure: onFailure.type,
	});

export const getDictionaryAction = () =>
	// onUserLogin({username, password})
	apiCall({
		url: dictionaryGetUrl,
		callParams: {
			method: httpMethods.get,
			headers: {
				...httpHeaders,
			},
			// credentials: "include",
		},
		onSuccess: onDictionaryGet.type,
		onFailure: onFailure.type,
	});

export const getAccountStatementAction = (startDate, endDate, accountNumber) =>
	apiCall({
		url: statementGetUrl,
		callParams: {
			method: httpMethods.post,
			headers: {
				...httpHeaders,
			},
			// credentials: "include",
			body: JSON.stringify({
				startDate,
				endDate,
				accountNumber
			}),
		},	
		onSuccess: onAccountStatementGet.type,
		onFailure: onFailure.type,
	});