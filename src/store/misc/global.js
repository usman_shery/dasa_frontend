export const pages = {
	dashboard: "Dashboard",
	dictionary: "Manage Parameters",
	accountStatement: "Account Statement"
};

export const validation = {
	isNull: (value) => value === undefined || value === null || value.length === 0,
	isUnderSize:
		(minLength) =>
		(value = "") =>
			value.length < minLength,
	isOverSize:
		(maxLength) =>
		(value = "") =>
			value.length > maxLength,
	isNotNumber: (value) => isNaN(value),
	hasSpaces: (value) => /\s/g.test(value),
};

export const serverUrl = window.serverUrls[window.env];

console.log(`Mode: ${window.env} | URL: ${serverUrl}`);
