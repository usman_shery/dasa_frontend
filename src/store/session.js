import { createSlice } from "@reduxjs/toolkit";
import { pages } from "./misc/global";

const initialState = {
	loading: false,
	loggedIn: false,
	activePage: pages.dashboard,
	dictionary: {
		values: undefined,
	},
	statement: {
		values: undefined,
	}
};

const slice = createSlice({
	name: "session",
	initialState,
	reducers: {
		onPageLoad: (state, action) => {
			return state;
		},


		onPageChange: (state, action) => {
			let page = action.payload.page;
			return {
				...state,
				activePage: page
			};
		},

		onUserLogin: (state, action) => {
			// console.log(action);
			if(action.payload.username !== "admin") {
				console.log("login false")
				return state;
			}

			return {
				...state,
				loggedIn: true
			}
		},

		onDictionaryGet: (state, action) => {
			// console.log(action);
			
			return {
				...state,
				dictionary: {
					values: action.payload
				}
			}
		},
		
		onAccountStatementGet: (state, action) => {
			console.log(action);
			
			return {
				...state,
				statement: {
					values: action.payload
				}
			}
		},
		// in case of any error
		onFailure: (state, action) => {
			console.log("Failure occured");
			console.log("Detail:", action);
			return state;
		},

	},
});

export const { onPageChange } = slice.actions;
export const { onUserLogin } = slice.actions;
export const { onDictionaryGet } = slice.actions;
export const { onAccountStatementGet } = slice.actions;
export const { onFailure } = slice.actions;
export default slice.reducer;
